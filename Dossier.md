#Productiedossier

##Briefing & Analyse

Maak een responsive mobile-first webapplicatie waarin minimaal 3 datasets (1x groep) - 6 datasets (2x groep) - 9 datasets (3x groep), afkomstig uit de dataset-pool van de Worldbank, verwerkt zijn. Conceptueel denken is heel belangrijk in deze applicatie. Dit betekent dat jullie verbanden moeten leggen tussen deze datasets. Het is louter niet alleen het oplijsten en visualiseren van deze datasets, er moet een concept rond gebouwd worden.

Deze applicatie is bestemd voor +18 jarigen waarbij de gebruiksvriendelijkheid, interactiviteit, uitbreidbaarheid en schaalbaarheid van belang zijn.

##Functionele specificaties
* One Page Webapplication
* Preloaders/Loaders
 + Om de app in te laden
 + Tijdens het inladen van JSON(P)
 + Lokale data bestanden
* De meeste inhoud wordt beheerd in data bestanden en wordt dynamisch ingeladen
* Adaptive afbeeldingen, video's en geluid
* Google Maps wordt integratie
* GEO-location
* Social Media Bookmarking
* Animaties via SVG en/of Canvas
* Lokaal caching van data en bronbestanden
* Gebruiker ervaart een interactief webapplicatie
* Gebruiker kan favoriete data lokaal bewaren
* Gebruiker kan de webapplicatie bookmarken in browser, bureaublad en als native app in het overzicht
* Automation verplicht!

##Technische specificaties
* Core technologies: HTML5, CSS3 en JavaScript
* Template engines: Jade, Haml, Swig of Handlebars
* Storage: JSON bestanden, localstorage en/of IndexedDB
* Bibliotheken: jQuery
* Geen backend
##Persona's
![](https://photos-2.dropbox.com/t/2/AAAfrstEzAnIp2ucQXMKEsvqkTREwZ9MPlohLis1QTKkOQ/12/102974039/png/32x32/1/_/1/2/User%20Persona.png/EJ7y--4DGGMgBygH/eSDHZGvyK_AUSFurVSX58kcEjoyRv0hCHw9LL-ASebc?size=1024x768&size_mode=2 "Persona's")

##Moodboard
####Versie 1 (final) 
![](https://photos-1.dropbox.com/t/2/AACavCQVTXeRboQMVa0D76MOjuKYrzwxAkz3c3m2XKkZMA/12/102974039/png/32x32/1/_/1/2/ideeënbord_1.png/EJ7y--4DGJwBIAEgBygH/UgoZfNKtCIir4PH__FlhDIJ_RPoVwCRMStw1Dqva0C8?size=1280x960&size_mode=2"Moodboard")
####Versie 2 (kleurenpalet)
![](https://photos-1.dropbox.com/t/2/AAAE74ELndBP8U9L7nR5P1mFGVjQIbnmnpobewiNNFTo5Q/12/102974039/png/32x32/1/_/1/2/moodboard_2.png/EJ7y--4DGHIgASAHKAc/81zTF8EphF_zjSBN-PC8u32MwD3Qzx_-ZUbb7mo6VWE?size=1280x960&size_mode=2 "Moodboard")
##Sitemap
![](https://photos-5.dropbox.com/t/2/AADtnbl871QDSD7lb3Hf7xcn_UzDEjn9E1VKZ4KHXgW4Sw/12/102974039/png/32x32/1/_/1/2/sitemap.png/EJ7y--4DGJwBIAEgBygH/IrTf526bN_xUGvwhojrpTbuw3XtjvZD7sN52NMfnW6s?size=1280x960&size_mode=2 "Sitemap")
##Wireframes Mobile
![](https://photos-4.dropbox.com/t/2/AABP1NoZ5ugwIZ0KVY7mYiRA3IwD3SfIOZ7A_ww70S5TBQ/12/102974039/jpeg/32x32/1/_/1/2/WireframeMobile-page-001.jpg/EJ7y--4DGLQBIAEgBygH/LcnXUbgZcYGT97F_ANsMGM-tdzBRaLyq5Lj5Fbwmf0c?size=1280x960&size_mode=2 "wireframe")
![](https://dl-web.dropbox.com/get/NMDADI/opdracht/Wireframes/WireframeMobile-page-002.jpg?_subject_uid=102974039&w=AABcLsZ9KQAZp0SLFIpgDhfwZ-QcbtdkPhFar9uFar_mMg "wireframe")
![](https://dl-web.dropbox.com/get/NMDADI/opdracht/Wireframes/WireframeMobile-page-003.jpg?_subject_uid=102974039&w=AACxD3FFYwfc0Vk29zyqR6QFJcnFQOLGNwyoUDRTMEzvJQ "wireframe")
![](https://dl-web.dropbox.com/get/NMDADI/opdracht/Wireframes/WireframeMobile-page-004.jpg?_subject_uid=102974039&w=AABtpbCUM1uyqtb0c2AmC_YIY6zBa1-JmvLK1vHSfqy7jQ "wireframe")
![](https://dl-web.dropbox.com/get/NMDADI/opdracht/Wireframes/WireframeMobile-page-005.jpg?_subject_uid=102974039&w=AAAF9zp8dFg00v7xZNdTOnjQiHBV-jIfoF8-m3aiEWFxbA "wireframe")
![](https://dl-web.dropbox.com/get/NMDADI/opdracht/Wireframes/WireframeMobile-page-006.jpg?_subject_uid=102974039&w=AAB10qDqyDlge4TPpP63mKg8gwlwMexICuV8ouvBYzhqbg "wireframe")
##Wireframes Desktop
####0.0 Home
![](https://photos-1.dropbox.com/t/2/AACrj63DWq7iHRHIKDqUto_ddF3QUkFP6Fo1vMkmT7mNHw/12/102974039/jpeg/32x32/1/_/1/2/WireframeDesktop-page-001.jpg/EJ7y--4DGLQBIAEgBygH/SXRJdg4IT5Fm8LjBF4up1smhaKng9-xPxl-C9W7bzss?size=1280x960&size_mode=2 "wireframe")
####1.0 Webapp
![](https://photos-5.dropbox.com/t/2/AADP2CJw2tSgbzgyf-aifUp6lMQYmHE7XVO2xbTY0v0QZA/12/102974039/jpeg/32x32/1/_/1/2/WireframeDesktop-page-002.jpg/EJ7y--4DGLQBIAEgBygH/ozFfhZHHcsrw-dm5RNj7X0txtG8uwkg6W0Hm0j3Ftmc?size=1280x960&size_mode=2 "wireframe")
####1.1 Webapp detail
![](https://photos-2.dropbox.com/t/2/AAAx8RCR8VqMo-55ySMfhiYNTWlZ2beGlEdSpiYEU35VsA/12/102974039/jpeg/32x32/1/_/1/2/WireframeDesktop-page-003.jpg/EJ7y--4DGLQBIAEgBygH/sDyS2Sjkwxi4rN9aI1iKz88kWPg2kvnwx5cxuaxYUqE?size=1280x960&size_mode=2 "wireframe")
####2.0 Webapp random
![](https://photos-1.dropbox.com/t/2/AADI6TRWLxlLrW-WjnRevBlE0SJ9sO0WjMflzFEluj2C3Q/12/102974039/jpeg/32x32/1/_/1/2/WireframeDesktop-page-004.jpg/EJ7y--4DGLQBIAEgBygH/CfZpLQU6LnpP85oyN3fpPaQ-AJfrXGOiWq9IH0Zlt3w?size=1280x960&size_mode=2 "wireframe")
####3.0 Contact
![](https://photos-6.dropbox.com/t/2/AACwX-vPxROtUJUCJsA2pe7zJAZMvLScm32pQqFFDmjNJQ/12/102974039/jpeg/32x32/1/_/1/2/WireframeDesktop-page-005.jpg/EJ7y--4DGLQBIAEgBygH/K0_UzMRbFDZM0jisYeNnF2kHf_Jse0MJ6LKN6MEMfPg?size=1280x960&size_mode=2 "wireframe")
####4.0 Disclaimer
![](https://photos-6.dropbox.com/t/2/AADnE_jl8oE3-y-SFyIH1Lc2H8Nitmjau5YsTu3ttfgcZw/12/102974039/jpeg/32x32/1/_/1/2/WireframeDesktop-page-006.jpg/EJ7y--4DGLQBIAEgBygH/VupcbAJjbBlARZ0b5Ahnn0PlE_oVHbuRlCmbWHdSS_4?size=1280x960&size_mode=2 "wireframe")
##Style Tiles
####Versie 1 (final)
![](https://photos-2.dropbox.com/t/2/AACNx6OZJGaR-4tRX13Kq5QDMMEZyo2bYjsspbE3TOlJng/12/102974039/png/32x32/1/_/1/2/Style_Tile_NMDAD_versie1.png/EJ7y--4DGHIgASAHKAc/ixceTKsqW8ClUxoK7UVZAbh9xPsM8vDPQXKDEL2tYgM?size=1280x960&size_mode=2 "Style tile")
####Versie 2
![](https://photos-3.dropbox.com/t/2/AABmjcR96pvQUPrsWFEAsjn3Z295EZhWsSCJ0JSo3qr85A/12/102974039/jpeg/32x32/1/_/1/2/Style_Tile_2.jpg/EJ7y--4DGJwBIAEgBygH/Z2Nhb4uEEWghDCvBli4RkB9WfX-uyZLyckXynybV-h0?size=1280x960&size_mode=2"Style tile")
##Tijdsbesteding per student
| Datum	     | Student            | Domein | Taak          | Tijd     |
| :--------- | :----------------- | :----: | :-----------: | :------: |
| 2015-10-28 | Ismet              | DES    | Markdown      | 1,00 uur |
| 2015-10-26 | Ismet              | DES    | Persona's     | 1,50 uur |
| 2015-10-27 | Ismet              | DES    | Moodboard     | 2,00 uur |
| 2015-10-27 | Ismet              | DES    | Style Tiles   | 1,00 uur |
| 2015-10-28 | Ismet              | DES    | Wireframes    | 1,00 uur |
| 2015-10-27 | Tristan            | DES    | Sitemap       | 0,50 uur |
| 2015-10-28 | Tristan            | DES    | Logo's        | 0,50 uur |
| 2015-10-27 | Tristan            | DES    | Moodboard     | 1,00 uur |
| 2015-10-28 | Tristan            | DES    | Style Tiles   | 1,00 uur |
| 2015-10-28 | Tristan            | DES    | Wireframes    | 1,50 uur |
| 2015-10-28 | Samen              | DES    | Tijdbesteding | 0,50 uur |
| 2015-10-28 | Samen              | DES    | Finals        | 1,00 uur |
| ---------- |    Alle studenten  | DES    | **Totaal** | **12,50 uur** |